#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 18:19:44 2018

@author: jimmy
"""

from scipy.sparse import csr_matrix
import Fisher
import scipy.io as sio
import numpy as np

filename = 'Test_1_mECS.mat'
X = csr_matrix(sio.loadmat(filename)['in_X']) #loading single-cell RNA-seq data

label = sio.loadmat(filename)['true_labs'] #this is ground-truth label for validation
X = X.toarray()
X_counts = np.round(np.power(10,X)-1)

FisherK = Fisher.Fisher_matrix(X_counts.T)