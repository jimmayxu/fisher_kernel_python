#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Functions for Fisher kernel based on zero inflated negative binomial distribution (ZINB)
    ---------------------------------------------------------------------
    This module contains the following functions:
    score_zinb 
    Calculate Fisher score based on konwn ZINB
    
    I_estimate
    Calculate empirical estimation of Fisher information based on known ZINB
    
    Fisher_zinb
    Calculate Fisher kernel between two obervations
    
    prior_zinb
    Provide a naive estimation of parameters of ZINB distribution on dataset
    
    MLE_zinb
    Provide maximum likelihood estimation of parameters ZINB distribution on dataset
    
    Fisher_matrix 
    A Fisher Kernel matrix, each entry (i,j) gives the evaluation of Fisher kernel between sample i and sample j
    
@author: jimmy
"""

import numpy as np
import scipy.special as ss
import warnings
from scipy.stats import norm,nbinom
from scipy.optimize import minimize



#    pm.traceplot(trace, varnames=['zinb'])
    
### Fisher kernel - Three parameter (mu, theta, pi) ###

# Fisher score of ZINB distribution
#input: y = np.array dim=1 or 2
def score_zinb(y , Theta):
    mu, theta, pi = Theta
    frac = theta/(theta+mu)
    f0 = pi + (1-pi)*np.power(frac,theta)
    if np.isscalar(y) :
        U = np.zeros(3)
        if y != 0:
            U[0] = y/mu - (y+theta)/(mu+theta)    
            U[1] = ss.digamma(y+theta) - ss.digamma(theta) + np.log(theta/(theta+mu)) + (mu-y)/(theta+mu)
            U[2] = -1/(1-mu)         
        else:
            U[0] = -(1-mu)*np.power(frac,(theta+1))/f0
            U[1] = (1-pi)*np.power(frac,theta)*(np.log(frac)+1-frac)/f0
            U[2] = (1-np.power(frac,theta))/f0  
        U = np.transpose(np.asmatrix(U))
    else:
        U = np.zeros([3,y.size])
        y = np.squeeze(np.asarray(y))

        
        U[0,:] = y/mu - (y+theta)/(mu+theta)    
        U[1,:] = ss.digamma(y+theta) - ss.digamma(theta) + np.log(theta/(theta+mu)) + (mu-y)/(theta+mu)
        U[2,:] = -1/(1-mu)         
        
        U[0,y==0] = -(1-mu)*np.power(frac,(theta+1))/f0
        U[1,y==0] = (1-pi)*np.power(frac,theta)*(np.log(frac)+1-frac)/f0
        U[2,y==0] = (1-np.power(frac,theta))/f0    
        U = np.asmatrix(U)
    return(U)
 



# Empirical estimate of Fisher information
def I_estimate(Theta, iteration=10000):
    mu, theta, pi = Theta
    x = np.random.negative_binomial(n=theta, p=theta/(mu+theta), size=iteration) * np.random.binomial(1,1-pi,size=iteration)
    I = np.zeros([3,3])
    
    U = score_zinb(x,Theta)
    for i in range(iteration):
        I += np.matmul(U[:,i],np.transpose(U[:,i]))
    return(I/iteration)




# Fisher kernel similarity metric based on ZINB 
#input: Y = np.array dim=1/2
def Fisher_zinb(Y , Theta, iteration = 10000, I = None):
    mu, theta, pi = Theta
    if I is None:
        I = I_estimate(Theta,iteration)
    L = np.linalg.cholesky(I)
    if np.min(np.asmatrix(Y).shape)==1 :
        Y = np.squeeze(np.asarray(Y))
        Fisher = np.matmul(np.transpose(np.linalg.solve(L,score_zinb(Y[0],Theta))) , np.linalg.solve(L,score_zinb(Y[1],Theta)))
        Fisher = np.asscalar(Fisher)
    else:
        score = np.zeros([3,Y.shape[0],2])
        score[:,:,0] = score_zinb(Y[:,0],Theta)
        score[:,:,1] = score_zinb(Y[:,1],Theta)
        Fisher = np.diagonal(np.matmul(np.transpose(np.linalg.solve(L,score[:,:,0])) , np.linalg.solve(L,score[:,:,1])))
    return(Fisher) #np.array

'''
### Fisher kernel - Two parameter (mu, theta) ### 


# Fisher score of ZINB distribution
def score_zinb2(y , Theta):
    mu, theta, pi = Theta
    frac = theta/(theta+mu)
    f0 = pi + (1-pi)*np.power(frac,theta)
    if np.isscalar(y) :
        U = np.zeros(2)
        if y != 0:
            U[0] = y/mu - (y+theta)/(mu+theta)    
            U[1] = ss.digamma(y+theta) - ss.digamma(theta) + np.log(theta/(theta+mu)) + (mu-y)/(theta+mu)
        else:
            U[0] = -(1-mu)*np.power(frac,(theta+1))/f0
            U[1] = (1-pi)*np.power(frac,theta)*(np.log(frac)+1-frac)/f0
        U = np.transpose(np.asmatrix(U))
    else:
        U = np.zeros([2,y.size])
        y = np.squeeze(np.asarray(y))
       
        U[0,:] = y/mu - (y+theta)/(mu+theta)    
        U[1,:] = ss.digamma(y+theta) - ss.digamma(theta) + np.log(theta/(theta+mu)) + (mu-y)/(theta+mu)

        U[0,y==0] = -(1-mu)*np.power(frac,(theta+1))/f0
        U[1,y==0] = (1-pi)*np.power(frac,theta)*(np.log(frac)+1-frac)/f0
        U = np.asmatrix(U)
    return(U)
 




# Empirical estimate of Fisher information
def I_estimate2(Theta, iteration=10000):
    mu, theta, pi = Theta
    x = np.random.negative_binomial(n=theta, p=theta/(mu+theta), size=iteration) * np.random.binomial(1,1-pi,size=iteration)
    I = np.zeros([2,2])
    
    U = score_zinb2(x,Theta)
    for i in range(iteration):
        I += np.matmul(U[:,i],np.transpose(U[:,i]))
    return(I/iteration)



# Fisher kernel similarity metric based on ZINB 
def Fisher_zinb2(Y , Theta, iteration = 10000, I = None):
    mu, theta, pi = Theta
    if I is None:
        I = I_estimate2(Theta,iteration)
    L = np.linalg.cholesky(I)
    if np.min(np.asmatrix(Y).shape)==1 :
        Y = np.squeeze(np.asarray(Y))
        Fisher = np.matmul(np.transpose(np.linalg.solve(L,score_zinb2(Y[0],Theta))) , np.linalg.solve(L,score_zinb2(Y[1],Theta)))
        Fisher = np.asscalar(Fisher)
    else:
        score = np.zeros([2,Y.shape[0],2])
        score[:,:,0] = score_zinb2(Y[:,0],Theta)
        score[:,:,1] = score_zinb2(Y[:,1],Theta)
        Fisher = np.diagonal(np.matmul(np.transpose(np.linalg.solve(L,score[:,:,0])) , np.linalg.solve(L,score[:,:,1])))
    return(Fisher)


'''

#### parameter estimation in ZINB #####


# A priori estimate 
# input: X = np.array with ndim 2,
def prior_zinb (X_counts):
    mu0 = np.apply_along_axis(lambda x: np.mean(x[x!=0]),1,X_counts)
    theta0 = np.square(mu0)/(np.apply_along_axis(np.var,1,X_counts)-mu0)
    pi0 = np.array(np.sum(X_counts == 0, axis = 1),dtype=float)/X_counts.shape[1]
    Theta = np.array([np.mean(mu0),np.mean(theta0),np.mean(pi0)])
    #Theta0_var = np.array([np.var(mu0),np.var(theta0),np.var(pi0)])
    return(Theta) # two arrays
    





# Maximum likelihood estimation 
# input: X = np.array ndim=1, Theta = np.array
def MLE_zinb (X, Theta0, detail = 0):
    
    def objective (phi):
        mu = np.exp(phi[0])
        theta = np.exp(phi[1])
        pi = norm.cdf(phi[2])

        
        f = n0*np.log(pi + (1-pi)*np.power(theta/(theta+mu),theta)) + \
        (n-n0)*np.log(1-pi)+ np.sum(np.log(nbinom.pmf(datt,n = theta, p = theta/(theta+mu))))
        return(-f)
    
    def gradient (phi):
        mu = np.exp(phi[0])
        theta = np.exp(phi[1])
        pi = norm.cdf(phi[2])

        
        ratio = theta/(theta+mu)
        power = np.power(ratio,theta)
        dmu = -n0*(1-pi) * \
            power*ratio/(pi + (1-pi)*power) + \
            np.sum(datt/mu - (datt+theta)/(mu+theta))
        dtheta = n0* (1-pi) * power *(np.log(ratio)+1-ratio)/(pi+(1-pi)*power) + \
            np.sum(ss.digamma(datt+theta) - ss.digamma(theta) + np.log(ratio) + (mu-datt)/(theta+mu))
        dpi = n0*(1-power)/(pi + (1-pi)*power) - \
            (n-n0)/(1-pi)
            
        dphi1 = dmu*np.exp(phi[0])
        dphi2 = dtheta*np.exp(phi[1])
        dphi3 = dpi*norm.pdf(phi[2])
        
        grad = np.array([-dphi1,-dphi2,-dphi3])
        return(grad)
    
    output = np.empty((0,3), float)
    
    warnings.filterwarnings("ignore")
    for l in range(X.shape[0]):
        dat = X[l,:]
        n0 = np.sum(dat==0)
        datt = dat[dat>0]
        n = dat.size
        
        mu0, theta0, pi0 = Theta0
        phi = np.array([np.log(mu0),np.log(theta0),norm.ppf(pi0)])
        #out = fmin_bfgs(objective, phi, fprime=gradient)
        #output[l] = [np.exp(out[0]),np.exp(out[1]), norm.cdf(out[2])]
        out = minimize(objective, phi, method='BFGS', jac=gradient,options={'disp': detail})
        Theta = np.array([np.exp(out.x[0]),np.exp(out.x[1]), norm.cdf(out.x[2])])
        output = np.vstack([output,Theta])
    
    warnings.filterwarnings("default")
    return(output) 
        
### Fisher matrix ####
#X.counts = np.array ndim=2
def Fisher_matrix (X_counts, ind = None, Theta0 = None, n_gene = 1000):
    n_cell = X_counts.shape[1]

    sort = np.argsort(np.apply_along_axis(np.var,1,X_counts))[::-1][100:(100+n_gene)]
    if Theta0 is None:
        Theta0 = prior_zinb(X_counts[sort,:]) #!!!!!!!double check
        if Theta0[1]<0:
            warnings.warn("A Priori parameter estimate of ZINB: Negative size parameter. Reset to 1.", DeprecationWarning)
            Theta0[1] = 1
    THETA = MLE_zinb(X_counts[sort,:] , Theta0)

    if ind is None:
        tempK = np.zeros([n_cell,n_cell])
        Fisher = np.zeros([n_cell,n_cell])
        for i,l in enumerate(sort):
            I = I_estimate(THETA[i,:])
            for j in range(n_cell):
                tempK[j,:] = Fisher_zinb(np.column_stack((X_counts[l,],[X_counts[l,j]]*n_cell)), THETA[i,:], I = I)
            Fisher += tempK    
    else:
        n_cell_reduce = ind.shape[1]
        tempK = np.zeros([n_cell , n_cell_reduce])
        Fisher = np.zeros([n_cell , n_cell_reduce])
        for i,l in enumerate(sort):
            I = I_estimate(THETA[i,:])
            for j in range(n_cell):
                tempK[j,:] = Fisher_zinb(np.column_stack((X_counts[l,ind[j,:]],[X_counts[l,j]]*n_cell_reduce)), THETA[i,:], I = I)
            Fisher += tempK
    
    return(Fisher/n_gene)


'''
Testing'
sample
X = np.random.negative_binomial(n=theta, p=theta/(mu+theta), size=iteration) * np.random.binomial(1,1-pi,size=iteration)
'''











