#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 14:42:55 2018

@author: jimmy
"""

#pip install rpy2, tzlocal
from rpy2.robjects import r, pandas2ri
pandas2ri.activate()
r.data('pbmc3k')
r['pbmc3k'].head()

pandas2ri.ri2py(r['pbmc3k'])




filename = 'Test_1_mECS.mat'
X = csr_matrix(sio.loadmat(filename)['in_X']) #loading single-cell RNA-seq data

label = sio.loadmat(filename)['true_labs'] #this is ground-truth label for validation
X = X.toarray()